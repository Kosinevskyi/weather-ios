//
//  MainWeatherModel.swift
//  Weather
//
//  Created by Roman Kosinevskyi on 10/11/19.
//  Copyright © 2019 Roman Kosinevskyi. All rights reserved.
//

import Foundation

class MainWeather: Codable {
    var temp: Float
    var temp_min: Float
    var temp_max: Float
}
