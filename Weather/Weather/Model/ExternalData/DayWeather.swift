//
//  DayWeatherModel.swift
//  Weather
//
//  Created by Roman Kosinevskyi on 10/11/19.
//  Copyright © 2019 Roman Kosinevskyi. All rights reserved.
//

import Foundation

class DayWeather: Codable {
    var main: MainWeather
    var weather: [WeatherDescription]
    var dt_txt: String
}
