//
//  WeatherDescription.swift
//  Weather
//
//  Created by Roman Kosinevskyi on 10/12/19.
//  Copyright © 2019 Roman Kosinevskyi. All rights reserved.
//

import Foundation

class WeatherDescription: Codable {
    var main: String
    var description: String
    var icon: String
}
