//
//  CityWeather.swift
//  Weather
//
//  Created by Roman Kosinevskyi on 10/12/19.
//  Copyright © 2019 Roman Kosinevskyi. All rights reserved.
//

import Foundation

class CityWeather: Codable {
    var name: String
}
