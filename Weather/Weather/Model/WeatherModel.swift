//
//  Weather.swift
//  Weather
//
//  Created by Roman Kosinevskyi on 10/11/19.
//  Copyright © 2019 Roman Kosinevskyi. All rights reserved.
//

import Foundation

class WeatherModel: Codable {
    var cod: String
    var message: Float
    var cnt: Int
    var list: [DayWeather]
    var city: CityWeather 
}
