//
//  ViewController.swift
//  Weather
//
//  Created by Roman Kosinevskyi on 10/10/19.
//  Copyright © 2019 Roman Kosinevskyi. All rights reserved.
//

import UIKit
import CoreLocation

class WeatherController: UIViewController {

    private var weatherView: WeatherView {
        return self.view as! WeatherView
    }
    
    private var newCityName: String = ""
    
    private var weatherModel: WeatherModel? {
        didSet {
            DispatchQueue.main.async {
                self.weatherModel?.city.name = self.newCityName
                
                self.navigationItem.title = self.weatherModel?.city.name
                self.navigationItem.hidesSearchBarWhenScrolling = true
                self.weatherView.vertivalTableView.reloadData()
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        weatherView.vertivalTableView.dataSource = self
        setupNavBar()
        
    }
    
    func setupNavBar() {
        //navigationItem.title = "Search weather in"
        navigationItem.title = NSLocalizedString("Search weather in", comment: "")
        
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.delegate = self
        
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
    }
}

extension WeatherController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        guard let city = searchBar.text,  !city.isEmpty else {
            print("User put no data!")
            return
        }

        CLGeocoder().geocodeAddressString(city) { (placemarks: [CLPlacemark]?, error: Error?) in
            if let location = placemarks?.first?.location {
                
                self.newCityName = city
                
                NetworkManager.shared.getWeather(lat: location.coordinate.latitude, lon: location.coordinate.longitude, units: "metric", result: { (weatherModel) in
                                self.weatherModel = weatherModel
                            })
            }
            
        }
    }
}

extension WeatherController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return weatherModel?.list.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return weatherModel?.list[section].dt_txt
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell") as! CustomTableViewCell
        let data = self.weatherModel?.list[indexPath.section]
        
        cell.weatherImage.image = UIImage(named: data?.weather[0].icon ?? "50d")
        cell.tempLabel.text = "\(Int(data?.main.temp ?? 0)) °C"
        
        //if let description = data?.weather[0].description {
            cell.descriptionLabel.text = NSLocalizedString(data?.weather[0].description ?? "", comment: "")
        //} else {
          //  cell.descriptionLabel.text = ""
        //}
        
        
        return cell
    }
}
