//
//  NetworkManager.swift
//  Weather
//
//  Created by Roman Kosinevskyi on 10/11/19.
//  Copyright © 2019 Roman Kosinevskyi. All rights reserved.
//

import Foundation

class NetworkManager {
    static let shared = NetworkManager()
    
    private let appID = "0633afc4771c8d6c0b3b516a6ba8fae3"
    
    func getWeather(lat: Double, lon: Double, units: String, result: @escaping ((WeatherModel?) -> ())) {
        requestURL(url: createURL(lat: lat, lon: lon, units: units), result: result)
        
    }
    
    func getWeather(city: String, units: String, result: @escaping ((WeatherModel?) -> ())) {
        requestURL(url: createURL(city: city, units: units), result: result)
    }
    
    private func createURL(lat: Double, lon: Double, units: String) -> URL? {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "api.openweathermap.org"
        urlComponents.path = "/data/2.5/forecast"
        urlComponents.queryItems = [URLQueryItem(name: "lat", value: String(lat)),
                                    URLQueryItem(name: "lon", value: String(lon)),
                                    URLQueryItem(name: "appid", value: appID),
                                    URLQueryItem(name: "units", value: units)]
        
        return urlComponents.url
    }
    
    private func createURL(city: String, units: String) -> URL? {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "api.openweathermap.org"
        urlComponents.path = "/data/2.5/forecast"
        urlComponents.queryItems = [URLQueryItem(name: "q", value: city),
                                    URLQueryItem(name: "appid", value: appID),
                                    URLQueryItem(name: "units", value: units)]
        
        return urlComponents.url
    }
    
    private func requestURL(url: URL?, result: @escaping ((WeatherModel?) -> ())) {
        
        guard let url = url else {
            print("url nil!!!")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let session = URLSession(configuration: .default)
        session.dataTask(with: request) { (data, response, error) in
            
            if error == nil {
                let decoder = JSONDecoder()
                
                guard let receivedData = data else {
                    print("No data received!")
                    return
                }
                
                var decoderModel: WeatherModel?
                
                decoderModel = try? decoder.decode(WeatherModel.self, from: receivedData)
                
                result(decoderModel)
                
            } else {
                print(error as Any)
            }
            
        }.resume()
    }
}
