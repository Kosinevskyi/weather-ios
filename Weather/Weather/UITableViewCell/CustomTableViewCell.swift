//
//  CustomTableViewCell.swift
//  Weather
//
//  Created by Roman Kosinevskyi on 10/14/19.
//  Copyright © 2019 Roman Kosinevskyi. All rights reserved.
//

import Foundation
import UIKit

class CustomTableViewCell: UITableViewCell {
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    
    override var reuseIdentifier: String? {
        return "CustomTableViewCell"
    }
}
