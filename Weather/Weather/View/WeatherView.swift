//
//  WeatherView.swift
//  Weather
//
//  Created by Roman Kosinevskyi on 10/13/19.
//  Copyright © 2019 Roman Kosinevskyi. All rights reserved.
//

import UIKit

class WeatherView: UIView {
    //var horizontalTableView = UITableView()
    
    var vertivalTableView = UITableView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initializer()
        setupConstraints()
    }
    
    private func initializer() {
        
        self.addSubview(vertivalTableView)
        vertivalTableView.register(UINib(nibName: "CustomTableViewCell", bundle: nil), forCellReuseIdentifier: "CustomTableViewCell")
    }
    
    private func setupConstraints() {
        vertivalTableView.translatesAutoresizingMaskIntoConstraints = false;

        vertivalTableView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        vertivalTableView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        vertivalTableView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        vertivalTableView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
}
